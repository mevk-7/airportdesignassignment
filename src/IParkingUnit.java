public interface IParkingUnit {
    ITerminal getTerminal();
    boolean isOccupied();
    ParkingType getType();
    String getPlaneId();
    void setOccupied(boolean occupied);
    void setPlaneId(String planeId);
}
