import java.util.List;

public interface IAirport {
    void addTerminal(ITerminal terminal);
    int getTotalNumTerminal();
    List<IParkingUnit> getParking(TypeOfFlight typeOfFlight);
    IGate requestForGate();
    void getDescription();

}
