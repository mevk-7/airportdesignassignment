public class Main {
    public static void main(String[] args) {

        Terminal t1 = Terminal.Factory.getTerminal("T-1",3,4,5);
        Airport ccu = new Airport("Kolkata",t1);
        ccu.addTerminal(Terminal.Factory.getTerminal("T-2",2,6,5));
        ccu.addTerminal(Terminal.Factory.getTerminal("T-3",5,8,6));

        ccu.getDescription();

        Plane domPlane1 = new Plane(TypeOfFlight.DOMESTIC,"DO-111");
        Plane intPlane1 = new Plane(TypeOfFlight.INTERNATIONAL, "IN-000");
        Plane priPlane1 = new Plane(TypeOfFlight.PRIVATE, "PR-999");

        System.out.println("\n");

        domPlane1.setAirport(ccu);
        domPlane1.landing();
        domPlane1.park();

        intPlane1.setAirport(ccu);
        intPlane1.landing();


        domPlane1.takeOff();

        intPlane1.park();
        intPlane1.takeOff();
    }
}
