import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;

public class Terminal implements ITerminal {

    private String terminalId;
    private List<IGate> listGate;

    private TreeSet<IParkingUnit> listPrivateParking;
    private TreeSet<IParkingUnit> listDomesticParking;
    private int numPrivateLot;
    private int numDomesticLot;
    private int numGates;

    private Terminal(String terminalId, int numPrivateLot, int numDomesticLot,int numGates) {
        this.terminalId = terminalId;
        this.listGate = new ArrayList<>();
        this.listPrivateParking = new TreeSet<>();
        this.listDomesticParking = new TreeSet<>();
        this.numPrivateLot = numPrivateLot;
        this.numDomesticLot = numDomesticLot;
        this.numGates = numGates;


        for (int i = 1;i<=this.numDomesticLot;i++)
        {
//            System.out.println(i);
            addDomesticParkingUnit(new ParkingUnit(terminalId+"D"+i, this, ParkingType.DOMESTIC));
        }
        for (int i = 1; i<=this.numPrivateLot;i++)
        {
            addPrivateParkingUnit(new ParkingUnit(terminalId + "P"+ i, this, ParkingType.PRIVATE));
        }
        for (int i = 1;i<=this.numGates;i++)
        {
            addGate(new Gate(this.terminalId+ "G"+ i,this));
        }

    }
    static  class  Factory{

        public static Terminal getTerminal(String terminalId, int numPrivateLot, int numDomesticLot,int numGates)
        {
            if (numPrivateLot + numDomesticLot <= 0 || numPrivateLot < 0 || numDomesticLot < 0)
            {
                System.out.println("Invalid no of parking lot has been passed");
                return null;
            }
            if (numGates <=0)
            {
                System.out.println("No of gates cannot be less than or equal to zero");
                return null;

            }
            return new Terminal(terminalId,numPrivateLot,numDomesticLot,numGates);
        }
    }



    /*
        TypeOfFlight takeoff and landing
     */

    /*
    Landing
     */
    public IGate requestForGate()
    {
        for (IGate gate: this.listGate)
        {
            if (!gate.isOccupied())
            {
                return gate;
            }
        }
        return null;
    }


    public  void leaveGate(Gate gate)
    {
        gate.setOccupied(false);

    }

    public void leaveParking(List<ParkingUnit> parkingList)
    {
        for (ParkingUnit unit:parkingList)
        {
            unit.setOccupied(false);
        }

    }

    public List<IParkingUnit> getParkingLot(TypeOfFlight typeOfFlight)
    {
        List<IParkingUnit> parkingLots = new ArrayList<>();
        int count;
        switch (typeOfFlight){
            case PRIVATE:
                //1 unit is required either private or domestic
                for (IParkingUnit park : this.listPrivateParking)
                {
                    if (!park.isOccupied())
                    {
                        park.setOccupied(true);
                        parkingLots.add(park);
                        return parkingLots;
                    }
                }
                for (IParkingUnit park : this.listDomesticParking)
                {
                    if (!park.isOccupied())
                    {
                        park.setOccupied(true);
                        parkingLots.add(park);
                        return parkingLots;
                    }
                }
                break;
            case DOMESTIC:
                //2 unit of private and 1 unit of domestic
                count = 0;
                for (IParkingUnit park : this.listDomesticParking)
                {
                    if (!park.isOccupied())
                    {
                        park.setOccupied(true);
                        parkingLots.add(park);
                        return parkingLots;
                    }
                }
                parkingLots.clear();
                for (IParkingUnit park: this.listPrivateParking)
                {
                    if (!park.isOccupied())
                    {

                        count++;
                        parkingLots.add(park);
                        if (count ==2)
                        {
                            for (IParkingUnit  unit: parkingLots)
                            {
                                unit.setOccupied(true);
                            }
                            return parkingLots;

                        }
                    }
                    else {
                        count = 0;
                        parkingLots.clear();


                    }
                }
                break;
            case INTERNATIONAL:
                //5 unit of domestic and 8 unit of private
                count = 0;

                for (IParkingUnit park : this.listPrivateParking)
                {
                    if (!park.isOccupied())
                    {
                        count ++;
                        parkingLots.add(park);

                        if (count == 8 )
                        {
                            for (IParkingUnit unit : parkingLots)
                            {
                                unit.setOccupied(true);

                            }
                            return  parkingLots;


                        }
                    }
                    else{
                        count = 0;
                        if (parkingLots.size()>0)
                            parkingLots.clear();
                    }
                }
                count = 0;
                parkingLots.clear();
                for (IParkingUnit park :this.listDomesticParking)
                {
                    if (!park.isOccupied())
                    {
                        count++;
                        parkingLots.add(park);
                        if (count == 5)
                        {
                            for (IParkingUnit unit: parkingLots)
                            {
                                unit.setOccupied(true);
                            }
                            return parkingLots;
                        }
                    }
                }

                break;
        }
        return  null;
    }

    public void addPrivateParkingUnit(ParkingUnit parkingUnit)
    {
        this.listPrivateParking.add(parkingUnit);
    }

    public void addDomesticParkingUnit(ParkingUnit parkingUnit)
    {
        this.listDomesticParking.add(parkingUnit);
    }

    public void addGate(Gate gate)
    {
        this.listGate.add(gate);

    }

    public int getNumberOFGates()
    {
        return this.listGate.size();
    }

    public int getNumPrivateLot()
    {
        return this.listPrivateParking.size();
    }
    public int getNumDomesticLot()
    {
        return this.listDomesticParking.size();
    }



    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Terminal("+this.terminalId+ ") with "
                + this.listGate.size() + " gates, "
                + this.listPrivateParking.size() + " private and "
                + this.listDomesticParking.size() + " domestic parking is added.\n" );

        for (IGate gate : this.listGate)
        {
            sb.append(gate.toString());
            sb.append("\n");
        }

        sb.append("\tPrivate Parking : \n");
        for (IParkingUnit park : this.listPrivateParking)
        {
            sb.append(park.toString());
        }
        sb.append("\tDomestic Parking :\n");
        for (IParkingUnit park : this.listDomesticParking)
        {
            sb.append(park);
        }
        sb.append("\n");

        return sb.toString();
    }


}
