public class Gate implements IGate {
    private String id ;
    private ITerminal terminal;
    private boolean isOccupied;

    public Gate(String id, ITerminal terminal) {
        this.id = id;
        this.terminal = terminal;
        this.isOccupied = false;

    }

    public boolean isOccupied() {
        return isOccupied;
    }

    public void setOccupied(boolean occupied) {
        isOccupied = occupied;
    }

    public String getId() {
        return id;
    }

    public ITerminal getTerminal() {
        return terminal;
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        return this.id.equals(((Gate)(obj)).id);
    }

    @Override
    public String toString() {
        return "\tGate{ " +
                "id = '" + id + "\' }";
    }
}
