import java.util.ArrayList;
import java.util.List;

public class Airport implements IAirport{

    private String name;
    private List<ITerminal> terminals;

//    private List<Gate>  gates;

    /*
    A airport at least have one terminal
     */
    public Airport(String name,ITerminal terminal) {
        this.name = name;
        this.terminals =  new ArrayList<>();
        this.terminals.add(terminal);
        System.out.println("\n:-- Welcome to " + this.name +  " Airpot --:");
    }

    public void addTerminal(ITerminal terminal)
    {
        terminals.add(terminal);
    }

    public int getTotalNumTerminal()
    {
        return this.terminals.size();
    }

    public List<IParkingUnit> getParking(TypeOfFlight typeOfFlight)
    {
        boolean granted =  false;
        for (ITerminal terminal: terminals)
        {
            List<IParkingUnit> parking = terminal.getParkingLot(typeOfFlight);
            if (parking != null)
            {
                granted = true;
                System.out.println("\t\t\t\tRequest granted, parking at: ");
                for (IParkingUnit unit:parking)
                {
                    System.out.print("\t\t"+ unit);
                }
                //System.out.println();
                return parking;
            }

        }

        System.out.println("\t\t\t\tRequest denied for parking. All parking unit busy. Request after sometime");
        return null;


    }




    public IGate requestForGate()
    {
        for (ITerminal terminal :terminals)
        {
            if (terminal.requestForGate() != null)
            {
                IGate gate = terminal.requestForGate();
                gate.setOccupied(true);
                return gate;
            }
        }
        return null;
    }


    public IGate requestToLand()
    {
        IGate gate = requestForGate();
        if (gate != null)
        {
            System.out.println("You have access to land");
            return gate;
        }
        System.out.println("No gate is available");
        return null;
    }



    public  void getDescription()
    {
        for (ITerminal terminal:terminals)
        {
            System.out.println("\n"+ terminal.toString());
        }
    }

    @Override
    public String toString() {
        return "Airport {" +
                "name='" + name + '\''+
                '}';
    }
}
