import java.util.List;

public class Plane {

    private TypeOfFlight flight;
    private String id;
    private IGate gate;
    private List<IParkingUnit> parkingUnits;
    private IAirport airport;

    public Plane(TypeOfFlight flight, String id) {
        this.flight = flight;
        this.id = id;
        System.out.println("A flight with id: [" + id + "] is created: " + flight);
    }


    public boolean takeOff()
    {
        boolean isTakeOff = false;

        if (gate!=null){
            gate.setOccupied(false);
            isTakeOff = true;
            System.out.println("Plane [" + id + "]: Boarding passenger");
            System.out.println("Plane [" + id + "]: Moving to runway from { Gate = " + gate.getId() + " }");
            gate = null;
        }
        else{
            System.out.println("Plane [" + id + "]: Requested for gate to board passengers");
            gate = getAirport().requestForGate();
            if (gate != null) {
                System.out.println("Plane [" + id + "]: Request granted, { Gate = " + gate.getId() + " }");
                System.out.println("Plane [" + id + "]: Boarding completed");
                gate.setOccupied(false);
                System.out.println("Plane [" + id + "]: Moving to runway from { Gate = " + gate.getId() + " }");
                isTakeOff = true;
            }
            gate = null;


        }
        if (isTakeOff)
            System.out.println("Plane [" + id + "]: Took off");
        return isTakeOff;

    }
    public boolean landing()
    {
        System.out.println("Plane [" + id + "]: Requesting for landing.");
        gate = getAirport().requestForGate();
        if (gate == null){
            System.out.println("Request denied for landing. All gate busy. Request after sometime");
            return false;
        }
        System.out.println("Plane [" + id + "]: Request granted, landing at: { Gate = " + gate.getId() + " }");
        System.out.println("Plane ["+ id + "]: Landed successfully, deboarding passengers.");
        return true;
    }

    public boolean park()
    {
        System.out.println("Plane ["+ id + "]: Deboarding of passengers completed");
        System.out.println("Plane ["+ id + "]: Requesting for parking");

        parkingUnits = getAirport().getParking(flight);

        if (parkingUnits!=null && gate!=null)
        {
            gate.setOccupied(false);
            gate = null;
            return true;
        }
        return false;
        //System.out.println("");

    }

    public TypeOfFlight getFlight() {
        return flight;
    }

    public void setFlight(TypeOfFlight flight) {
        this.flight = flight;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public IGate getGate() {
        return gate;
    }

    public void setGate(IGate gate) {
        this.gate = gate;
    }

    public List<IParkingUnit> getParkingUnits() {
        return parkingUnits;
    }

    public void setParkingUnits(List<IParkingUnit> parkingUnits) {
        this.parkingUnits = parkingUnits;
    }

    public IAirport getAirport() {
        return airport;
    }

    public void setAirport(IAirport airport) {
        this.airport = airport;
        System.out.println("Plane [" + id + "]: Set to land on " + airport.toString());
    }


}
