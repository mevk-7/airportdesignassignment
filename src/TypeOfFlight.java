public enum TypeOfFlight {
    INTERNATIONAL,
    DOMESTIC,
    PRIVATE
}
