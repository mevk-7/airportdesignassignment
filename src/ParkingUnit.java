import java.util.Objects;

enum ParkingType{
    PRIVATE,
    DOMESTIC
}
public class ParkingUnit implements Comparable,IParkingUnit{
    private String id;
    private ITerminal terminal;
    private boolean isOccupied;
    private ParkingType type;
    private String planeId;

    public ParkingUnit(String id, ITerminal terminal, ParkingType type) {
        this.id = id;
        this.terminal = terminal;
        this.isOccupied = false;
        this.type = type;
        this.planeId = null;
    }

    public ITerminal getTerminal() {
        return terminal;
    }

    public boolean isOccupied() {
        return isOccupied;
    }

    public ParkingType getType() {
        return type;
    }

    public String getPlaneId() {
        return planeId;
    }

    public void setOccupied(boolean occupied) {
        isOccupied = occupied;
    }

    public void setPlaneId(String planeId) {
        this.planeId = planeId;
    }

    @Override
    public boolean equals(Object o) {

        return this.id.equals(((ParkingUnit)o).id);
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override
    public String toString() {
        return "\t\tParkingUnit {" +
                "id = '" + id + "\'"  +

                ", isOccupied = " + isOccupied +
                ", type = " + type +

                "}\n";
    }

    @Override
    public int compareTo(Object o) {
        return this.id.compareTo(((ParkingUnit)o).id);
    }
}


