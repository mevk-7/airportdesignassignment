import java.util.List;

public interface ITerminal {
    public void addGate(Gate gate);
    public List<IParkingUnit> getParkingLot(TypeOfFlight typeOfFlight);
    public void leaveParking(List<ParkingUnit> parkingList);
    public IGate requestForGate();
    public  void leaveGate(Gate gate);




}
