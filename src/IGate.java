public interface IGate {

    boolean isOccupied();
    void setOccupied(boolean occupied);
    String getId();
    ITerminal getTerminal();



}
