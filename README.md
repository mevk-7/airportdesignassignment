#Airport System Design 

###Language -  JAVA

This module offers following classes : 

    Airport - While creating airport instance you must provide name of airport 
    and one terminal instance .For example Airport airport  =  new Airport("Airport name",
    terminal_object)
    Airport.description will print all description of airport including airport name,
    terminal description, gates and parking spots.
    
    Airport.addTerminal will add Terminal instance to airport
     
    
    
    Terminal  =  Terminal instance can be created by Terminal.Factory method with parameters
    :name of terminal
    :no of private parking spot
    :no of domestic parking spot
    :no of gates in terminal
    
    
    Plane : For creating plane instance we must provide type of flight and plane name
    Plane.setAirport will set airport for landing and take off purpose
    plane.takeoff will handle takeoff functionality
    plane.landing will handle landing functionality
    plane.park will handle parking functionality
    
    
    TypeOfFlight (enum) with type
    :DOMESTIC 
    :PRIVATE
    :INTERNATIONAL
    
    
            
    
