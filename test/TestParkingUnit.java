import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.awt.*;
import java.util.Iterator;

import static junit.framework.TestCase.*;

public class TestParkingUnit {

    private static ITerminal mock;

    @BeforeClass
    public static   void setup()
    {
        System.out.println("Before class");
         mock = Mockito.mock(ITerminal.class);

    }


    @Test
    public void testIsOccupied()
    {
//        fail();
        ParkingUnit unit = new ParkingUnit("123",mock,ParkingType.PRIVATE);
        assertFalse(unit.isOccupied());

        unit.setOccupied(true);
        assertTrue(unit.isOccupied());

        unit.setOccupied(false);
        assertFalse(unit.isOccupied());

    }


    @Test
    public void testgetType()
    {
        ParkingUnit unit = new ParkingUnit("123",mock,ParkingType.DOMESTIC);
        assertEquals(unit.getType(),ParkingType.DOMESTIC);

        assertNotSame(unit.getType(),ParkingType.PRIVATE);
    }

}
