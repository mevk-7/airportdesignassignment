import org.junit.Test;
import org.mockito.Mockito;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;

public class TestGate {

    @Test
    public void testGateEquals(){
        ITerminal mock = Mockito.mock(ITerminal.class);
        Gate gate2 = new Gate("T1",mock);
        Gate gate1 = new Gate("T1",mock);
        assertTrue(gate1.equals(gate2));


        Gate gate3 = new Gate("T2",mock);
        assertFalse(gate3.equals(gate1));

    }
    @Test
    public void testGateGetTerminals()
    {
//        fail();
        ITerminal mock = Mockito.mock(ITerminal.class);
        Gate gate1 = new Gate("T1", mock);

        assertEquals(mock,gate1.getTerminal());

    }

}
