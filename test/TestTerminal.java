import org.junit.Test;
import static org.junit.Assert.*;

public class TestTerminal {

    @Test
    public void testAddFunction()
    {
//        fail();
        Terminal terminal =  Terminal.Factory.getTerminal("T3",3,4,5);
        assertEquals(terminal.getNumberOFGates(),5);
        assertEquals(terminal.getNumDomesticLot(),4);
        assertEquals(terminal.getNumPrivateLot(),3);

        assertNotEquals(terminal.getNumDomesticLot(),6);


    }

    @Test
    public void testGetParkingLot()
    {
        /*
        Test whether class allot parking lot when we have free space
        Space:
            8 --Domestic
            4 --private
         Sequence:
            1--Domestic
            1--Private
            1--International
         */

//        fail();
        Terminal terminal =  Terminal.Factory.getTerminal("Test",8,8,4);


        assertNotNull(terminal.getParkingLot(TypeOfFlight.DOMESTIC));
        assertNotNull(terminal.getParkingLot(TypeOfFlight.PRIVATE));
        assertNotNull(terminal.getParkingLot(TypeOfFlight.INTERNATIONAL));
    }

    @Test
    public void testgetParkingLot1()
    {
        /*
            5 -- Private
            4 - Domestic

            Seq --- 4 Domestic
                --- 4 Private
                --- 1 International
                should give null
         */


//        fail();
        Terminal terminal =  Terminal.Factory.getTerminal("T3",5,4,6);

        for (int i =0;i<4;i++)
            assertNotNull(terminal.getParkingLot(TypeOfFlight.DOMESTIC));

        for (int i =0;i<4;i++)
            assertNotNull(terminal.getParkingLot(TypeOfFlight.PRIVATE));

        assertNull(terminal.getParkingLot(TypeOfFlight.INTERNATIONAL));

    }





    @Test
    public void testgetParkingLot2()
    {
        /*
            8 -- Private
            6 - Domestic

            Seq --- 4 Domestic
                --- 2 Private
                --- 1 International
                should give null
         */


//        fail();
        Terminal terminal =  Terminal.Factory.getTerminal("T3",8,6,6);

        for (int i =0;i<4;i++)
            assertNotNull(terminal.getParkingLot(TypeOfFlight.DOMESTIC));

        for (int i =0;i<2;i++)
            assertNotNull(terminal.getParkingLot(TypeOfFlight.PRIVATE));

        assertNull(terminal.getParkingLot(TypeOfFlight.INTERNATIONAL));

    }

    @Test
    public void testgetParkingLot3()
    {
        /*
            8 -- Private
            7 - Domestic

            Seq --- 5 Private
                --- 2 Domestic
                --- 1 International
                should not give null
         */


//        fail();
        Terminal terminal =  Terminal.Factory.getTerminal("T3",8,7,6);

        for (int i =0;i<5;i++)
            assertNotNull(terminal.getParkingLot(TypeOfFlight.PRIVATE));

        for (int i =0;i<2;i++)
            assertNotNull(terminal.getParkingLot(TypeOfFlight.DOMESTIC));

        assertNotNull(terminal.getParkingLot(TypeOfFlight.INTERNATIONAL));



    }


    @Test
    public void testgetParkingLot4()
    {
        /*
            10 -- Private
            10 - Domestic

            Seq --- 6 Domestic
                --- 2 Private
                --- 1 International
                --- 1 Private
                should not give null
         */


//        fail();
        Terminal terminal =  Terminal.Factory.getTerminal("T3",10,10,6);

        for (int i =0;i<6;i++)
            assertNotNull(terminal.getParkingLot(TypeOfFlight.DOMESTIC));

        for (int i =0;i<2;i++)
            assertNotNull(terminal.getParkingLot(TypeOfFlight.PRIVATE));

        assertNotNull(terminal.getParkingLot(TypeOfFlight.INTERNATIONAL));

        assertNotNull(terminal.getParkingLot(TypeOfFlight.PRIVATE));



    }


    @Test
    public void testCreation()
    {
//        fail();
        assertNull(Terminal.Factory.getTerminal("T3",-1,2,8));
        assertNull(Terminal.Factory.getTerminal("T3",1,-2,8));
        assertNull(Terminal.Factory.getTerminal("T3",0,0,8));
        assertNull(Terminal.Factory.getTerminal("T3",1,2,-8));
        assertNull(Terminal.Factory.getTerminal("T3",1,2,0));


    }




}
