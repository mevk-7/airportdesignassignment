import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.junit.Assert.*;

public class TestAirport {


    @Test
    public void testAddAirport()
    {
//        fail();
        ITerminal terminal1 = Mockito.mock(ITerminal.class);
        ITerminal terminal2 = Mockito.mock(ITerminal.class);
        ITerminal terminal3 = Mockito.mock(ITerminal.class);

        Airport airport = new Airport("123",terminal1);
        airport.addTerminal(terminal2);
        airport.addTerminal(terminal3);

        assertEquals(3,airport.getTotalNumTerminal());

    }


    @Test
    public void testLanding()
    {

//        fail();
        ITerminal terminal1 = Mockito.mock(ITerminal.class);
        ITerminal terminal2 = Mockito.mock(ITerminal.class);
        ITerminal terminal3 = Mockito.mock(ITerminal.class);

        Gate dummyGate1 = Mockito.mock(Gate.class);
//        Gate dummyGate2 = Mockito.mock(Gate.class);
//        Gate dummyGate3 = Mockito.mock(Gate.class);


        Airport airport = new Airport("123",terminal1);
        airport.addTerminal(terminal2);
        airport.addTerminal(terminal3);

        assertNull(airport.requestToLand());
        Mockito.when(terminal1.requestForGate()).thenReturn(dummyGate1);
        assertNotNull(airport.requestToLand());




    }




}
