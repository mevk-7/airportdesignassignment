
import org.junit.Before;
import org.junit.Test;

import org.mockito.Mockito;
import  org.mockito.*;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.*;

public class TestPlane {

    IAirport airport;
    ITerminal terminal;
    IGate dummyGate;


    @Before
    public void beforeEachTest()
    {
        airport = Mockito.mock(IAirport.class);
        terminal =  Mockito.mock(ITerminal.class);
        dummyGate = Mockito.mock(IGate.class);
    }
    @Test
    public void testLanding()
    {


        Plane plane = new Plane(TypeOfFlight.DOMESTIC,"cdb-340");
        plane.setAirport(airport);

        assertFalse(plane.landing());

        Mockito.when(terminal.requestForGate()).thenReturn(dummyGate);
        Mockito.when(airport.requestForGate()).thenReturn(dummyGate);


        assertTrue(plane.landing());




    }
    @Test
    public void  testTakeOff()
    {
        Plane plane = new Plane(TypeOfFlight.DOMESTIC,"rdn-345");
        plane.setAirport(airport);
        plane.setGate(dummyGate);



        assertTrue(plane.takeOff());

        plane.setGate(null);
        Mockito.when(airport.requestForGate()).thenReturn(null);

        assertFalse(plane.takeOff());



    }


    @Test
    public void testParking()
    {
//        fail();

        Plane plane = new Plane(TypeOfFlight.DOMESTIC, "eds-234");
        plane.setGate(dummyGate);
        plane.setAirport(airport);

        Mockito.when(airport.getParking(plane.getFlight())).thenReturn(null);

        assertFalse(plane.park());

        IParkingUnit dummyParking  =  Mockito.mock(IParkingUnit.class);
        List<IParkingUnit> dummyParkingList = new ArrayList<>();
        dummyParkingList.add(dummyParking);

        Mockito.when(airport.getParking(plane.getFlight())).thenReturn(dummyParkingList);

        assertTrue(plane.park());







    }


}
